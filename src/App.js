import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView, StatusBar, Alert} from 'react-native';
import {Provider} from 'react-redux';
// import LoadingView from './src/components/LoadingView';
// import {setContainer} from './src/services/navigatorService'
import {store} from './redux/store';
// import {PersistGate} from 'redux-persist/integration/react'

//Navigation
import Navigation from './Navigation';

export default function App() {
  console.disableYellowBox = true;
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>


    // <Provider store={store}>
    //   <PersistGate persistor={persistor}>
    //     <LoadingView>
    //       <Navigation
    //       ref={ref => {
    //         setContainer(ref);
    //       }}/>
    //     </LoadingView>
    //   </PersistGate>
    // </Provider>
  );
}
