import 'react-native-gesture-handler';
import React from 'react';
import {Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Icons} from '../utils';

//Screens
import Home from "../Screens/Home";
import Wallpaper from "../Screens/Wallpaper";
import Detail from "../Screens/Detail";
import Loading from "../Screens/Loading";
import Login from "../Screens/Login";
import Register from "../Screens/Register";
import KYC from "../Screens/KYC";
import ForgetPassword from "../Screens/ForgetPassword";
import ChangePassword from "../Screens/ChangePassword";
import Notification from "../Screens/Notification";
import Profile from "../Screens/Profile";




const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function AppHome() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: 'rgb(85,136,231)',
        style: {height: 65, borderTopRightRadius: 20, borderTopLeftRadius: 20},
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => {
            if (focused == true) {
              return <Image source={Icons.homeIcon} />;
            } else if (focused == false) {
              return <Image source={Icons.homeIcon2} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Pharmacy"
        component={Pharmacy}
        options={{
          tabBarLabel: 'Pharmacy',
          tabBarIcon: ({focused}) => {
            if (focused == true) {
              return <Image source={Icons.pharmacyIcon2} />;
            } else if (focused == false) {
              return <Image source={Icons.pharmacyIcon} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Reports"
        component={Reports}
        options={{
          tabBarLabel: 'Reports',
          tabBarIcon: ({focused}) => {
            if (focused == true) {
              return <Image source={Icons.recordsIcon2} />;
            } else if (focused == false) {
              return <Image source={Icons.recordsIcon} />;
            }
          },
        }}
      />
      <Tab.Screen
        name="Video"
        component={Video}
        options={{
          tabBarLabel: 'Video',
          tabBarIcon: ({focused}) => {
            if (focused == true) {
              return <Image source={Icons.videoIcon2} />;
            } else if (focused == false) {
              return <Image source={Icons.videoIcon} />;
            }
          },
        }}
      />
    </Tab.Navigator>
  );
}

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SplashScreen"
        screenOptions={{
          headerShown: false,
        }}>
        
        <Stack.Screen name="Loading" component={Loading}/>
        <Stack.Screen name="Home" component={Home}/>
        <Stack.Screen name="Detail" component={Detail}/>
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="Register" component={Register}/>
        <Stack.Screen name="ForgetPassword" component={ForgetPassword}/>
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;
