import React from "react";
import { 
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Dimensions
} from "react-native";
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import All from "./components/All";
import Model from "./components/Model";
import Assignment from "./components/Assignment";

export default class Home extends React.Component{
  render(){
    return(
      <View style={styles.container}>
          <View style={styles.header}>
              <ImageBackground
              source={require("../../asset/header.png")}
              style={styles.imageBackground}
              resizeMode="contain"
              >
                  <Text style={styles.title}>Hello</Text>
              </ImageBackground>
          </View>
          <View style={styles.tabbar}>
            <ScrollableTabView
              initialPage={0}
              tabBarActiveTextColor="green"
              renderTabBar={() => <DefaultTabBar
                underlineStyle={{
                  backgroundColor:'green'
                }} />}
            >
              <All tabLabel="All" props={this.props} />
              <Model tabLabel="Models" props={this.props} />
              <Assignment tabLabel="Assignments" props={this.props} />

            </ScrollableTabView>
          </View>
      </View>
    )
  }
}

const width = Dimensions.get("screen").width;

var styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'white'
  },
  header: {
    position:'absolute'
  },
  tabbar: {
    flex:1,
    marginTop: width*0.3,
    paddingHorizontal:30
  },
  imageBackground: {
    width: width*0.4,
    height: width*0.4,
    alignItems:'center'
  },
  title: {
    color:'white',
    marginTop:25,
    fontWeight:'bold',
    fontSize:25
  }
});