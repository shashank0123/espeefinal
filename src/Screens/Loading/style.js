import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const styles = {
    splash_bg:{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: height,
        width: width,

    },
    splashLogo: {
        marginTop: height*0.3,
        marginBottom: height*0.40,
        height: height*0.3,
        width: width*0.6,
        alignSelf: 'center',
    }
}

export default styles