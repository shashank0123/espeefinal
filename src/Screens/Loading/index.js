import React, {Component} from 'react';
import {View, Text, Image } from 'react-native';
import {Images} from '../../utils';
import styles from './style'

class Loading extends Component{
	componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Login')
        }, 2500)
    }
    render () {
        return (
            <View>
                <View style={styles.splashbg}>
                <Image 
                      source={Images.splash}
                      
                    />
                </View>
            </View>
            );
    }
}



export default Loading;