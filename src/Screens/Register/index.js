import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import {Form, Item, Body, Input,CheckBox, Button} from 'native-base';
import styles from './style';

import {Icons, Images} from '../../utils';

//Libraries
import LinearGradient from 'react-native-linear-gradient';

import { login } from '../../redux/actions/login';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-ionicons'
import { LoginButton, AccessToken } from 'react-native-fbsdk';

class Register extends Component{
	componentDidMount(){
		// this.props.login()
	}

	state = {
        icon: "eye-off",
        password: true
    };

    _changeIcon() {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            password: !prevState.password
        }));
    }
	render() {
		return (
			<View style={styles.container}>
				<ScrollView>
				<View style={styles.wrapper}>
					<Icon name="ios-arrow-round-back" />	      	      
				</View>

				<View style={styles.title}>
					<Text style={styles.signin}>Register</Text>
				</View>
				<View style={styles.formArea}>
					<Form style={styles.mainForm}>
						<Item style={styles.formItems}>

							<Input placeholder="First Name" style={styles.Input} />
							<Input placeholder="Last Name" style={styles.Input} />
						</Item>
						<Item style={styles.formItems}>

							<Input placeholder="Email" style={styles.Input} />
						</Item>

						<Item style={styles.formItems}>

							<Input placeholder="Phone" style={styles.Input} />
						</Item>
						<Item style={styles.formItems} floatingLabel>
			                <Input placeholder={'Password'} secureTextEntry={this.state.password} style={styles.Input} onChangeText={(e) => onChange(e)} />
			                <Icon name={this.state.icon} onPress={() => this._changeIcon()} />
			            </Item>         
						<View style={styles.Button}>
							<Button block style={styles.mainBtn}>
								<Text style={styles.btnText}>Register</Text>
							</Button>
						</View>
					</Form>
				</View>

				<View style={styles.socialLoginArea}>
					<Text style={styles.simpleText}>Or login with Social Account</Text>
					<View style={styles.socialBtnContainer}>
						<Button block style={[styles.socialBtn, styles.googlebtn]}>
							<Text style={styles.btnText1}>Google</Text>
						</Button>
						<LoginButton
					          onLoginFinished={
					            (error, result) => {
					              if (error) {
					                console.log("login has error: " + result.error);
					              } else if (result.isCancelled) {
					                console.log("login is cancelled.");
					              } else {
					                AccessToken.getCurrentAccessToken().then(
					                  (data) => {
					                    console.log(data.accessToken.toString())
					                  }
					                )
					              }
					            }
					          }
					          onLogoutFinished={() => console.log("logout.")}/>
					</View>
				</View>
				<View style={styles.signupArea}>
					<Text style={styles.signup}>Already have an account? Login</Text>
				</View>
				</ScrollView>
			</View>

			);
	}
}
const mapStateToProps = (state) => {
	return {
		login: state.login.login,
	};
};

export default connect(mapStateToProps, {
	login,
})(Register);
