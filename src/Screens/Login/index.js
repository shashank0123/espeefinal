import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import {Form, Item, Body, Input,CheckBox, Button} from 'native-base';
import styles from './style';

import {Icons, Images} from '../../utils';

//Libraries
import LinearGradient from 'react-native-linear-gradient';

import { login } from '../../redux/actions/login';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-ionicons'

class Login extends Component{
  componentDidMount(){
    // this.props.login()
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Icon name="ios-arrow-round-back" />                
        </View>

        <View style={styles.title}>
          <Text style={styles.signin}>Log In</Text>
        </View>
        <View style={styles.formArea}>
          <Form style={styles.mainForm}>
            <Item style={styles.formItems}>
              <Input placeholder="Username" style={styles.Input} />
            </Item>
            <Item style={styles.formItems}>
              <Input placeholder="Password" style={styles.Input} />
            </Item>           
            <View style={styles.Button}>
              <Text style={styles.fBtn}>Forgot Password</Text>
              <Button block style={styles.mainBtn}>
                <Text style={styles.btnText}>Log In</Text>
              </Button>
            </View>
          </Form>
        </View>

        <View style={styles.socialLoginArea}>
          <Text style={styles.simpleText}>Or login with Social Account</Text>
          <View style={styles.socialBtnContainer}>
            <Button block style={[styles.socialBtn, styles.googlebtn]}>
              <Text style={styles.btnText1}>Google</Text>
            </Button>
            <Button block style={[styles.socialBtn, styles.fbbtn]}>
              <Icon style={styles.socialIcon} name="logo-facebook" />
              <Text style={styles.btnText1}>Facebook</Text>
            </Button>
          </View>
        </View>
        <View style={styles.signupArea}>
          <TouchableOpacity onPress={this.props.navigation.navigate('Register')}>
            <Text style={styles.signup}>Dont have an account? Register</Text>
          </TouchableOpacity>
        </View>
      </View>

      );
  }
}
const mapStateToProps = (state) => {
  return {
    login: state.login.login,
  };
};

export default connect(mapStateToProps, {
  login,
})(Login);
