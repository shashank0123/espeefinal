import {StyleSheet, Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    paddingTop:'6%',
    paddingLeft:'8%',
    backgroundColor:'#fff'
  },
  wrapper:{
    
  },
  title:{
    marginTop:20
  },
  signin:{
    fontSize:28

  }, 
  formItems: {
    marginTop: 15,
    paddingRight:'10%',
    borderBottomColor: '#2D3057',
  },
  Input: {
    fontFamily: 'Poppins-Bold',
    fontSize: 12,

  },
  formArea: {
    alignSelf: 'center',
    position:'relative',
    marginRight:'10%',
    width: '100%',
    top: '10%',
    paddingBottom: 40,
  },

  socialLoginArea: {
    alignItems: 'center',
    position:'relative',
    marginRight:'10%',
    width: '100%',
    top: '10%',
    paddingBottom: 40,
  },
  signupArea: {
    alignItems: 'center',
    margin:'auto',
    position:'absolute',
    marginRight:'10%',
    width: '100%',
    bottom: '2%',
    paddingBottom: 40,
  },
  fBtn:{
    alignSelf:'flex-end',
    paddingTop:10,
    paddingBottom:20,

  },
  mainBtn: {
    width: '100%',
    color: '#fff',
    borderRadius:30,
    height: 40,
    backgroundColor:'red'
  },
  btnText: {

    fontFamily: 'GoogleSans-Medium',
    fontSize: 20,
    color:'#fff',
  },

  btnText1: {

    fontFamily: 'GoogleSans-Medium',
    fontSize: 15,
  },
  simpleText:{
    marginBottom:15,
  },
  socialBtn:{
    width: '100%',
    backgroundColor: '#fff',
    color: '#000',
    borderRadius:30,
    height: 40,
    borderColor:'#000',
    borderWidth:1,
    marginRight:10,

    // borderShadow:'#000'
  },
  googlebtn:{
    display:'flex',
    flexDirection:'row',
    flex:1
  },
  fbbtn:{
    display:'flex',
    flexDirection:'row',
    flex:1,
    width:10
  },
  socialIcon:{
    width:30
  },
  socialBtnContainer:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',

  },
  signup:{
    position:'absolute',
    bottom:5
  }
});
