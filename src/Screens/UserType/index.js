import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, ScrollView} from 'react-native';
import {Form, Item, Input, Body, CheckBox, Button} from 'native-base';
import styles from './style';

import {Icons, Images} from '../../utils';

//Libraries
import LinearGradient from 'react-native-linear-gradient';
 
import { login } from '../../redux/actions/login';
import { connect } from 'react-redux';
import { TextInput } from 'react-native-gesture-handler';

class Register extends Component{
componentDidMount(){
    // this.props.login()
  }
  render() {
    return (
      	      <View style={styles.container}>
        <View style={styles.top}></View>

        <View style={styles.middle}>
          <Text style={styles.textContainer}>You are ready to go</Text>
          
          <View style={styles.formArea}>
            <Text style={[styles.textContainer, styles.signin]}>Sign in</Text>
            <Form style={styles.mainForm}>
              <Item style={styles.formItems}>
                <Input placeholder="Username" style={styles.Input} />
              </Item>
              <Item style={styles.formItems}>
                <Input placeholder="Password" style={styles.Input} />
              </Item>           
              <View style={styles.Button}>
                <Button block style={styles.mainBtn}>
                  <Text style={styles.btnText}>Submit</Text>
                </Button>
              </View>
            </Form>
          </View>

          <View style={styles.otherBtns}>
          <TouchableOpacity onPress={this.props.navigation.navigate('ForgetPassword')}><Text style={styles.fBtn}>Forgot Password ? </Text></TouchableOpacity>
          <TouchableOpacity onPress={this.props.navigation.navigate('Register')}><Text style={styles.Sbtn}>Signup </Text></TouchableOpacity>
          </View>
         
        </View>
        <View style={styles.bottom}></View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    login: state.login.login,
  };
};

export default connect(mapStateToProps, {
  login,
})(Register);
